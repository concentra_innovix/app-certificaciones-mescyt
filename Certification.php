
<?php
require_once('backend.php');

$title = 'hola mundo';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo utf8_encode($no_solicitud.'-'.$titulo);?></title>

    <style>
       body{
           margin: 3%;
       }
    </style>
</head>
<body>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <center>
        <h2><u>CERTIFICACION DE <?= utf8_encode($titulo);?></u></h1>
        
    </center>
    <br/>
    <h3>Número de documento: <?php echo $no_documento?><br/> Solicitud No. <span style="margin-left: 75px;">:</span> <?php echo $no_solicitud?></h3>
    <br/>
    <p align="justify"><?php echo $certificacion?></p>
    
    <p align="justify">Se expide la presente certificación a solicitud de la parte interesada, en Santo Domingo de Guzmán, Distrito Nacional, capital de la República Dominicana, <b><?php echo $dia?></b>  del mes de <b><?php echo $mes?></b> del año <b><?php echo $ano?></b>.</p>
    <br/>
    <center><h3><?php echo utf8_encode($firma)?><br/>DIRECTOR DE CONTROL ACADEMICO</h3></center>
    <br/><br/>
    <img alt="testing" src="<?php echo $url?>"/>
    <h5><b>NOTA: Esta certificación será válida, siempre y cuando no sufra borraduras ni alteraciones en su contenido y si presenta los sellos de la institución.</b></h4>
</body>
</html>