<?php

require_once('connection.php');
require_once('NumeroALetras.php');
require_once 'lib/nusoap.php';
require "phpqrcode/qrlib.php"; 
$no_solicitud = $_GET["id"];
$cat = $_GET["cat"];
$id_cert = $_GET["idcert"];
$id_doc = $_GET["iddoc"];
//query DOCUMENT
$consultaDOC = "SELECT TITLECERT,DOCUMENTO   FROM dynMESCyTDTL01 Docs JOIN dynMescyTDTL02 certmodel ON Docs.CATEGORIA = certmodel.cdcat 
where Docs.SOLICITUDNUMERO = '$no_solicitud' AND IDDOCUMENTO = '$id_doc'";
$ejecutarDOC = sqlsrv_query($conn, $consultaDOC);
$filaDOC = sqlsrv_fetch_array($ejecutarDOC);


//query CERTIFICATION
$consulta = "SELECT * FROM DYNMESCyTHE03 where SOLICITUDNUMERO = '$no_solicitud'";
$ejecutar = sqlsrv_query($conn, $consulta);
$fila = sqlsrv_fetch_array($ejecutar);

//query ATTRIBUTE
$consultaAtribute = "SELECT NOMBRECOMPLETO,Carr.CARRERANOMBRE, Ies.ARTICULO, Ies.IESNOMBRE, Ies.IESSIGLAS FROM DYNMESCyTHE03 Sol LEFT JOIN dynMESCyTCL07 Carr ON Carr.oid = Sol.OIDABCT7YGE2DQ7VXF LEFT JOIN DYNMESCyTCL05 Ies ON IES.oid = Sol.OIDABCXBQ  where Sol.SOLICITUDNUMERO = '$no_solicitud'";
$ejecutarAtribute = sqlsrv_query($conn, $consultaAtribute);
$filaAtribute = sqlsrv_fetch_array($ejecutarAtribute);


//arreglos
$array_data = explode(",",$fila['VLATTRICERT']);
$dc_legalizado = $filaDOC['DOCUMENTO'];
$titulo = strtoupper($filaDOC['TITLECERT']);
$no_documento =  $id_cert;
$code = SeachCode($no_documento,$conn);


//Datos de la base de datos
$nombre_ciudad = $filaAtribute['NOMBRECOMPLETO'];
$carrera = $filaAtribute['CARRERANOMBRE'];
$articulo =  $filaAtribute['ARTICULO'];
$ies = $filaAtribute['IESNOMBRE'];
$siglas = $filaAtribute['IESSIGLAS'];
$fecha_titulo = $fila['FECHATITULO'];
$firma = $fila['FIRMAAUT'];
$no_titulo = $fila['NUMEROTITULO'];
$dttituloacadem = $fila['DTTITULOACADEM'];
//este metodo genera el qr
$url = qr($code, $titulo);

//almacena y escribe en letra la fechas de la certificacion
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$dia = 'a los '.ucwords(strtolower(NumeroALetras::convertir(date("d")))).'('.date("d").') días';
$mes = $meses[date('n')-1];
$ano = ucwords(strtolower(NumeroALetras::convertir(date("Y")))).'('.date("Y").')';

// variables para cambiar el parrafo de certificacion
$certificacion = '';

$certificacion1 = 'Certificamos que la certificación de <strong class="letra font-weight-bold">'.utf8_encode($dc_legalizado).'</strong> anexa, a nombre de <strong class="letra font-weight-bold">'.utf8_encode($nombre_ciudad).'</strong>, de la
carrera <strong>'.utf8_encode($carrera).'</strong>, fue expedida por '.utf8_encode($articulo).' <strong class="letra font-weight-bold">'.utf8_encode($ies).' ('.utf8_encode($siglas).')</strong> y la misma cumple con todos los
requisitos establecidos por este Ministerio; por lo cual es considerada
un documento bueno y válido.';

$certificacion2 = 'Certificamos que el <strong class="letra font-weight-bold">'.utf8_encode($dc_legalizado).'</strong> a nombre de <strong class="letra font-weight-bold">'.utf8_encode($nombre_ciudad).'</strong> , de la carrera de <strong class="letra ">'.utf8_encode($carrera).'</strong>, fue expedida por '.utf8_encode($articulo).'
<strong class="letra font-weight-bold">'.utf8_encode($ies).'('.utf8_encode($siglas).')</strong> y la misma cumple con todos los requisitos establecidos por este
Ministerio; por lo cual se considera un documento bueno y válido.';

$certificacion3 = 'Certificamos que el <strong class="letra font-weight-bold">'.utf8_encode($dc_legalizado).'</strong> a nombre de <strong class="letra font-weight-bold">'.utf8_encode($nombre_ciudad).'</strong>, de la carrera de <strong class="letra ">'.utf8_encode($carrera).'</strong>, registrado en <strong class="letra font-weight-bold">'.utf8_encode($dttituloacadem).'</strong> expedido por '.utf8_encode($articulo).' <strong class="letra font-weight-bold">'.utf8_encode($ies).'('.utf8_encode($siglas).')</strong>, cumple con todos los requisitos
establecidos en el plan de estudio de la carrera; por lo cual se
considera un documento bueno y válido.';

    if(substr($cat, -2)=='01'){
        $certificacion = $certificacion1;
    }else if(substr($cat, -2)=='02'){
        $certificacion = $certificacion2;
    }else{
        $certificacion = $certificacion3;
    }






//busca el codigo del documento
function SeachCode($id,$conn)
{
    $data = "SELECT cddocument FROM dcdocrevision docrev where docrev.IDDOCUMENT = '$id'";
    $ejecutar = sqlsrv_query($conn, $data);
    $fila = sqlsrv_fetch_array($ejecutar);
    
    $CDdoc = $fila['cddocument'];
    return $CDdoc;
}


function qr($code, $titulo){
	//Declaramos una carpeta temporal para guardar la imagenes generadas
	$dir = 'temp/';
	
	//Si no existe la carpeta la creamos
	if (!file_exists($dir))
        mkdir($dir);
	
        //Declaramos la ruta y nombre del archivo a generar
	$filename = $dir.'test.png';
 
        //Parametros de Condiguración
	
	$tamaño = 4; //Tamaño de Pixel
	$level = 'L'; //Precisión Baja
	$framSize = 2; //Tamaño en blanco
	$contenido = 'https://se.servicios.gob.do/se/document/dc_view_document/api_view_document.php?cddocument="'.$code.'"&nmfile="'.$titulo.'".pdf';; //Texto
	
        //Enviamos los parametros a la Función para generar código QR 
    QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
    return $dir.basename($filename);
}



?>