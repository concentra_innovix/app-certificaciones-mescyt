<?php

require_once 'lib/nusoap.php';
header('Content-Type: text/html; charset=utf-8');
$wsdl = 'https://se.servicios.gob.do/softexpert/webserviceproxy/se/ws/dc_ws.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CERTIFCACIONES - MESCYT</title>
    <!-- Font Awesome -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<style>
#btn{
    background-color: red;
}
.letra{
    font-family: 'Source Sans Pro', sans-serif;
}
.float{
    position: absolute;
    left: 40%;
    top: -5px;
    z-index: 1;
    background-color: #CA2231;
     border-color: #CA2231;
     color: white;
     margin: auto;
    width: 20%
     
     
}
.central{
    position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  padding: 90px;
  padding-bottom:  20px;
}
button:hover {
    background-color: #991407;
    border-color: #991407;
}
</style>

</head>
<body>
<form action="validation.php" method="post" >

<div class="container central" >
   
<div class="card text-white   mb-3" >
        <div style="background-color: #FFFFFF;" class="card-header text-center">
            <img src="logo1.png" width="500" alt=""> 
        </div>
        <div style="background-color: #0B5692;  " class="card-body">


<?php
//trae datos desde category.php para las rutas
$array = $_POST['url'];
foreach ($array as $key => $value) {
    //evalua si hay datos
    if($value['ruta']){
        //valida si existe el documento en el diretorio
        if (file_exists($value['ruta'])) {
            echo '<h5 class="letra text-left green-text"><b class="white-text">['.$value['title'].']</b> subido correctamente</h5>';
            //metodo para subir archivos y retorna detalle de la funcion
            echo "<label>DETALLE: ".utf8_encode(UploadFile($value['id'],$value['ruta'],$wsdl,$value['title']))."</label><br>";
        } else {
            ?>
            
            <!--genera codigo de los input donde se muestran los campos con las url que no se han subidos-->
            <div style="padding: 10px;">
                <h5 class="letra text-left red-text" ><b class="white-text" >CERTIFICACION DE <?php echo $value['title'] ?></b> no pudo ser subido</h5>
                
                <div class=""> 
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text"   name="fileUpload<?php echo $key?>"   value="<?php echo $value['ruta']?>"  class="form-control"  disabled>
                            
                        </div>
                        <div class="col-md-1">
                        
                        <div class="row">
                        
                        <a style="color: white" target="_blank" href="<?php echo $value['rutaCertificacion']?>"><i class="fa fa-eye fa-2x" style="margin-left: -10px; margin-top: -5px" aria-hidden="true"></i></a>
                        
                        </div>
                        <div class="row">
                        <label style="margin-left: -7px; font-size: 13px; margin-top: -5px">subir</label>

                        </div>
                          
                            
                        </div>
                    </div>    
                </div>
                </div>
                
            
            
            
            
            
            
            
            <?php
        }
    }else{
        echo 'no llegaron datos';
    }
    print_r('<br>');
}

function UploadFile($doc,$ruta,$wsdl,$title){
            $nombre = str_replace(' ','_',$title);
        $b64Doc = chunk_split(base64_encode(file_get_contents($ruta))); 

        $client = new nusoap_client($wsdl,false);        // Connect the SOAP server
        $client -> setCredentials("splws01","splws01");

        $items = array('NMFILE'=>$nombre.'.pdf','BINFILE'=> $b64Doc, 'ERROR'=>'');
        $file = array('item'=>$items);
        $params = array('IDDOCUMENT' => $doc,'IDREVISION'=>'00','IDUSER' =>'','FILE'=>$file);
        $response = $client->call('uploadEletronicFile',$params);
        //print_r($client);
        
        
        return $response ;

}
?>


 <br>
        <div class="col-md-12">
        
        
        <a class=" float btn  btn-lg" onclick="recargar()">Verificar</a>
        
        </div>
        
             
        </div>
</div>
                       
</div>
</form>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
    <script>
        
        function eso(id, label) {
            var data = document.getElementById(id).value;
            var path = '';
            path = data.replace("C:\\fakepath\\", "");
            document.getElementById(label).innerHTML = path;
        }

        function recargar() {
            location.reload();
        }
        
    </script>
  
</body>
</html>