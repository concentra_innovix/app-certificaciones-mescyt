<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CERTIFCACIONES - MESCYT</title>
    <!-- Font Awesome -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<style>
#btn{
    background-color: red;
}
.letra{
    font-family: 'Source Sans Pro', sans-serif;
}
.float{
    position: absolute;
    left: 40%;
    z-index: 1;
    margin: auto;
    background-color: #CA2231;
     border-color: #CA2231;
     color: white;
     width: 20%
     
     
}
.central{
    position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  padding: 90px;
}
button:hover {
    background-color: #991407;
    border-color: #991407;
}
</style>

</head>
<body>
<form action="validation.php" method="post" >

<div class="container central" >
   
<div class="card text-white   mb-3"  >
        <div style="background-color: #FFFFFF;" class="card-header text-center">
            <img src="logo1.png" width="500" alt=""> 
        </div>
        <div style="background-color: #0B5692;  " class="card-body">
        
<?php

require_once 'lib/nusoap.php';
require_once('connection.php');
//id por parametro url get
$no_solicitud = $_GET["id"];

//consulta para generar los distintos documento
$consulta = "SELECT IDCATCERT,IDDOCUMENTO,TITLECERT,DOCUMENTO   FROM dynMESCyTDTL01 Docs JOIN dynMescyTDTL02 certmodel ON Docs.CATEGORIA = certmodel.cdcat 
where Docs.SOLICITUDNUMERO = '$no_solicitud'";
$ejecutar = sqlsrv_query($conn, $consulta);
$c = 0;
$url = [];
while($fila = sqlsrv_fetch_array($ejecutar)){
    
    $category = $fila['IDCATCERT'];
   
    $title = $fila['TITLECERT'];
    $doc = $fila['DOCUMENTO'];
    $IDdoc = $fila['IDDOCUMENTO'];
    $idCertificacion = newDocument($category, $title, $no_solicitud);
    AsociateDocument($idCertificacion,$no_solicitud);
            echo '<script>
                        window.open("Certification.php?id='.$no_solicitud.'&cat='.$category.'&idcert='.$idCertificacion.'&iddoc='.$IDdoc.'");
                </script>';
    $url[$c] ='C:\\MESCYT_CERTIFICACIONES\\'.$no_solicitud.'-'.$title.'.pdf';
    
?>
<!--genera html para hacer los input iguardar en el array url[] las rutas-->
           <div style="padding: 10px;">
                <h5 class="letra text-left" ><b>CERTIFICACION DE <?php echo $title ?></b></h5>
                
                <div class=""> 
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text"   name="fileUpload<?php echo $c?>"   value="<?php echo $url[$c]?>"  class="form-control"  disabled>
                            <input type="hidden"   name="url[<?php echo $c?>][ruta]"   value="C:\\MESCYT_CERTIFICACIONES\\<?php echo $no_solicitud.'-'.$title.'.pdf'?>"  class="form-control"  >
                            <input type="hidden"   name="url[<?php echo $c?>][rutaCertificacion]"   value="Certification.php?id=<?php echo $no_solicitud?>&cat=<?php echo $category?>&idcert=<?php echo $idCertificacion?>&iddoc=<?php echo $IDdoc?>"  class="form-control">
                            <input type="hidden"   name="url[<?php echo $c?>][title]"   value="<?php echo $title ?>"  class="form-control">
                            <input type="hidden"   name="url[<?php echo $c?>][id]"   value="<?php echo $idCertificacion ?>"  class="form-control">
                        </div>
                        <div class="col-md-1">
                        
                        <div class="row">
                        
                        <i class="fa fa-eye fa-2x" style="margin-left: -10px; margin-top: -5px" aria-hidden="true"></i>
                        
                        </div>
                        <div class="row">
                        <label style="margin-left: -3px; font-size: 13px; margin-top: -5px">pdf</label>

                        </div>
                          
                            
                        </div>
                    </div>    
                </div>
                </div>
                
               
 <?php   
 $c++;            
}
//Metodo para asociar documento
function AsociateDocument($iddoc,$WorkflowID)
{
    $dc_custom = new nusoap_client('http://192.168.153.69:81/CustomServicesSE/SeIntegrations.php',false);
    $params = array('DocumentsID'=>$iddoc,'WorkFlowId'=>$WorkflowID,'ActivityId'=>'MescytLDA1009','UsersCuantity'=>5);
    $dc_custom->setCredentials('splws01','splws01');
    $request = $dc_custom->call('AsociateMultipleDocs',$params);
}

// metodo para crear documentos
function newDocument($category, $title, $no_solicitud)
{
    $atrib = 'Mescytnumsoli='.$no_solicitud.';Mescytestadodocum=Legalizado';
    $dc_ws = new nusoap_client('https://se.servicios.gob.do/softexpert/webserviceproxy/se/ws/dc_ws.php',false);
    
    $dc_ws->setCredentials('splws01','splws01');
    $params = array('idcategory' => $category,'IDDOCUMENT'=>'','title' =>'CERTIFICACION DE '. $title,'DSRESUME'=>'','DTDOCUMENT'=>'','ATTRIBUTES'=>$atrib);
    $request = $dc_ws ->call('newDocument',$params);
    $separado = explode(":", $request);
    $DocumentID = $separado[0];
      
    return $DocumentID;
}

?>
          
   
        <br>
        <div class="col-md-12">
      
         <input type="submit"  class=" float btn  btn-lg" value="Subir"/>
        
        </div>
        
             
        </div>
</div>
                       
</div>

</form>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
    <script>
        
        function eso(id, label) {
            var data = document.getElementById(id).value;
            var path = '';
            path = data.replace("C:\\fakepath\\", "");
            document.getElementById(label).innerHTML = path;
        }
        
    </script>
  
</body>
</html>